package com.pixel.rtg.main.sample;

import javafx.application.Application;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import com.pixel.rtg.main.model.Point;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static javafx.collections.ListChangeListener.Change;


public class Main extends Application {

    private String regex = "[0-9]{1,3}\\.{1}[0]{1}|[0-9]{1,3}";
    private ObservableList<Point> pointList = prepareObservableList();
    private ListProperty<Point> listProperty;
    private Pane root;
    private Integer maxNumberOfPoints = 10;
    private List<Integer> rgbList = fillRgbList();
    private VBox vbox;
    private Circle draggedCircle = new Circle();
    private ObservableList<Node> rootChildren;

    private ObservableList<Point> prepareObservableList() {
        listProperty = new SimpleListProperty<>();
        pointList = FXCollections.observableArrayList();
        listProperty.set(pointList);
        return pointList;
    }

    private List<Integer> fillRgbList() {
        List<Integer> rgbList = new ArrayList<>();
        for (int i = 1; i < 255; i++) {
            rgbList.add(i);
        }
        return rgbList;
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/com/pixel/rtg/main/resources/fxml/RTGMainScreenView.fxml"));
        root = loader.load();
        rootChildren = root.getChildren();
        initializeBaseEvents();
        initializeMouseEvents();


        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        primaryStage.setTitle("RTG Diagnostic Application");
        primaryStage.setResizable(false);

        createPointListOnView();

        pointList.addListener(this::onChanged);

        primaryStage.show();

    }

    private void initializeMouseEvents() {

        for (int i = 0; i < rootChildren.size(); i++) {
            Node childPane = rootChildren.get(i);
            if (!(childPane instanceof VBox)) {
                Node directPane = ((Pane) childPane).getChildren().get(0);
                if (directPane instanceof Pane) {
                    directPane.setOnMouseClicked(
                            event -> {
                                if (pointList.size() <= maxNumberOfPoints) {
                                    if (event.getButton() == MouseButton.PRIMARY) {
                                        Node vBox = root.getChildren().get(0);
                                        if (vBox instanceof VBox) {
                                            ((VBox) vBox).getChildren().clear();
                                        }
                                        if (event.isStillSincePress()) {
                                            Node sourceNode = (Node) event.getSource();
                                            event.getTarget();
                                            placePoint(event, sourceNode);
                                        }
                                    }
                                }
                                if (event.getButton() == MouseButton.SECONDARY) {
                                    Node pickedCircle = event.getPickResult().getIntersectedNode();
                                    if (pickedCircle instanceof Circle) {
                                        for (int j = 0; j < pointList.size(); j++) {
                                            if (pointList.get(j).getCircle().getFill()
                                                    .equals(((Circle) pickedCircle).getFill())) {
                                                removePoints((Circle) pickedCircle);
                                                removePoints((Circle) pickedCircle);
                                                removePoints((Circle) pickedCircle);
                                                removePoints((Circle) pickedCircle);
                                                pointList.remove(pointList.get(j));
                                            }

                                        }

                                        ((Pane) directPane).getChildren().remove(pickedCircle);
                                    }
                                    ((Pane) directPane).getChildren();
                                }
                                event.consume();
                            }
                    );
                }
            }

        }
    }

    private void initializeBaseEvents() {
        for (int a = 1; a < rootChildren.size(); a++) {
            Node rootChild = rootChildren.get(a);
            if (rootChild instanceof Pane) {
                List<Node> nodes = ((Pane) rootChild).getChildren();
                for (int i = 0; i < nodes.size(); i++) {
                    if (nodes.get(i) instanceof Pane) {
                        nodes.get(i).setOnDragOver(e -> {
                            e.acceptTransferModes(TransferMode.MOVE);
                        });
                        Node providedPane = nodes.get(i);
                        if (providedPane instanceof Pane) {
                            providedPane.setOnDragDropped(e -> {
                                for (int j = 0; j < pointList.size(); j++) {
                                    if (pointList.get(j).getCircle().getFill().equals(draggedCircle.getFill())) {
                                        Point point = new Point(pointList.get(j));
                                        point.setX(e.getX());
                                        point.setY(e.getY());
                                        pointList.set(j, point);

                                        for (int k = 1; k < rootChildren.size(); k++) {
                                            Node child = rootChildren.get(k);
                                            if (child instanceof Pane) {
                                                Node directPane = ((Pane) child).getChildren().get(0);
                                                ObservableList<Node> directPaneChildren = ((Pane) directPane).getChildren();
                                                for (Node cicrle : directPaneChildren) {
                                                    if (cicrle instanceof Circle) {
                                                        if (((Circle) cicrle).getFill().equals(draggedCircle.getFill())) {
                                                            cicrle.setTranslateX(e.getX());
                                                            cicrle.setTranslateY(e.getY());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                e.setDropCompleted(true);
                            });
                        }
                    }
                }
            }
        }
    }


    private void removePoints(Circle picked) {
        for (int i = 1; i < rootChildren.size(); i++) {
            Node rootChild = rootChildren.get(i);
            if (rootChild instanceof Pane) {
                Node directPane = ((Pane) rootChild).getChildren().get(0);
                ObservableList<Node> children = ((Pane) directPane).getChildren();
                for (int j = 0; j < children.size(); j++) {
                    Node child = children.get(j);
                    if (child instanceof Circle) {
                        if (((Circle) child).getFill().equals(picked.getFill())) {
                            children.remove(child);
                        }
                    }
                }
            }
        }
    }


    private void onChanged(Change change) {
        while (change.next()) {
            if (change.wasRemoved()) {
                vbox.getChildren().clear();
                createPointListOnView();
            } else if (change.wasAdded()) {
                createPointListOnView();
                ObservableList<Node> children = vbox.getChildren();
                for (Node child : children) {
                    if (child instanceof Pane) {
                        Node xTextField = ((Pane) child).getChildren().get(2);
                        Node yTextField = ((Pane) child).getChildren().get(4);

                        ((TextField) xTextField).textProperty().addListener((obs, oldText, newText) -> {
                            Paint textfill = null;
                            Parent parent = xTextField.getParent();
                            Label pointLabel = (Label) ((Pane) parent).getChildren().get(0);
                            textfill = pointLabel.getTextFill();
                            for (Point point : pointList) {
                                if (point.getCircle().getFill().equals(textfill)) {
                                    if (newText.matches(regex))
                                        if (Double.parseDouble(newText) < Point.getMaxX()) {
                                            point.getCircle().setTranslateX(Double.parseDouble(newText));
                                            for (int i = 1; i < rootChildren.size(); i++) {
                                                Node providedPane = rootChildren.get(i);
                                                if (providedPane instanceof Pane) {
                                                    Pane directPane = (Pane) ((Pane) providedPane).getChildren().get(0);
                                                    ObservableList<Node> directPaneChildren = directPane.getChildren();
                                                    for (Node directPaneChild : directPaneChildren) {
                                                        if (directPaneChild instanceof Circle) {
                                                            if (((Circle) directPaneChild).getFill().equals(point.getCircle().getFill())) {
                                                                directPaneChild.setTranslateX(point.getCircle().getTranslateX());
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if (oldText.matches(regex)) {
                                                point.getCircle().setTranslateX(Double.parseDouble(oldText));
                                            }
                                        }
                                }
                            }
                        });

                        ((TextField) yTextField).textProperty().addListener((obs, oldText, newText) -> {
                            Paint textfill = null;
                            Parent parent = yTextField.getParent();
                            Label pointLabel = (Label) ((Pane) parent).getChildren().get(0);
                            textfill = pointLabel.getTextFill();
                            for (Point point : pointList) {
                                if (point.getCircle().getFill().equals(textfill)) {
                                    if (newText.matches(regex))
                                        if (Double.parseDouble(newText) < Point.getMaxY()) {
                                            point.getCircle().setTranslateY(Double.parseDouble(newText));
                                            for (int i = 1; i < rootChildren.size(); i++) {
                                                Node providedPane = rootChildren.get(i);
                                                if (providedPane instanceof Pane) {
                                                    Pane directPane = (Pane) ((Pane) providedPane).getChildren().get(0);
                                                    ObservableList<Node> directPaneChildren = directPane.getChildren();
                                                    for (Node directPaneChild : directPaneChildren) {
                                                        if (directPaneChild instanceof Circle) {
                                                            if (((Circle) directPaneChild).getFill().equals(point.getCircle().getFill())) {
                                                                directPaneChild.setTranslateY(point.getCircle().getTranslateY());
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if (oldText.matches(regex)) {
                                                point.getCircle().setTranslateY(Double.parseDouble(oldText));
                                            }
                                        }
                                }
                            }
                        });
                    }
                }
            }
        }
    }


    private void placePoint(MouseEvent event, Node sourceNode) {
        Circle circle1 = createPointShape(event);
        Paint fillOfPoint = circle1.getFill();
        Paint strokeOfPoint = circle1.getStroke();
        Point point;

        for (int i = 1; i < rootChildren.size(); i++) {
            Node node = rootChildren.get(i);
            Node node11 = ((Pane) node).getChildren().get(0);
            if (node11.equals(sourceNode)) {
                if (node11 instanceof Pane) {
                    Circle circle = createPointShape(event);
                    circle.setFill(fillOfPoint);
                    circle.setStroke(strokeOfPoint);
                    point = new Point(event.getX(), event.getY(), circle);
                    point.getCircle().setOnDragDetected(e -> {
                        Object source = e.getSource();
                        if (source instanceof Circle) {
                            draggedCircle = (Circle) source;
                            Dragboard db = circle.startDragAndDrop(TransferMode.MOVE);
                            db.setDragView(circle.snapshot(null, null));
                            ClipboardContent cc = new ClipboardContent();
                            cc.putString("circle");
                            db.setContent(cc);
                        }
                    });
                    pointList.add(point);
                    ((Pane) node11).getChildren().add(point.getCircle());
                }
            }

            for (int j = 1; j < rootChildren.size(); j++) {
                Node node1 = rootChildren.get(i);
                if (!((Pane) node1).getChildren().get(0).equals(sourceNode)) {
                    if (rootChildren.get(i) instanceof Pane) {
                        Node node22 = ((Pane) node1).getChildren().get(0);
                        if (node22 instanceof Pane) {
                            Circle circle = createPointShape(event);
                            circle.setFill(fillOfPoint);
                            circle.setStroke(strokeOfPoint);
                            circle.setOnDragDetected(e -> {
                                Object source = e.getSource();
                                if (source instanceof Circle) {
                                    draggedCircle = (Circle) source;
                                }
                                Dragboard db = circle.startDragAndDrop(TransferMode.MOVE);
                                db.setDragView(circle.snapshot(null, null));
                                ClipboardContent cc = new ClipboardContent();
                                cc.putString("circle");
                                db.setContent(cc);
                            });
                            ((Pane) node22).getChildren().add(circle);

                        }
                    }
                }

            }

        }
    }


    private Circle createPointShape(MouseEvent event) {

        Collections.shuffle(rgbList);
        Circle circle = new Circle();
        circle.setRadius(4);
        int r = rgbList.get(0);
        rgbList.add(r);
        rgbList.remove(0);
        int g = rgbList.get(0);
        rgbList.add(g);
        rgbList.remove(0);
        int b = rgbList.get(0);
        rgbList.add(b);
        rgbList.remove(0);

        Color pointsColor = Color.rgb(r, g, b);

        circle.setFill(pointsColor);
        circle.setStroke(Color.valueOf("black"));

        double width = event.getX();
        double height = event.getY();

        if (width < Point.getMaxX()) {
            circle.setTranslateX(width);
        }
        if (height < Point.getMaxY()) {
            circle.setTranslateY(height);
        }

        return circle;
    }


    private void createPointListOnView() {
        for (Point point : pointList) {

            Pane pane = new Pane();
            pane.setPadding(new Insets(20, 20, 20, 20));
            pane.setStyle("-fx-border-color: gray;");
            pane.setMaxHeight(110);

            Label title = new Label();
            title.setStyle(" -fx-font-weight: bold; -fx-font-size: 20 ");
            title.setLayoutX(0);
            title.setTextFill(point.getCircle().getFill());
            title.setText(point.getName());

            Label x = new Label();
            x.setStyle(" -fx-font-weight: bold; -fx-font-size: 20 ");
            x.setLayoutX(20);
            x.setLayoutY(40);
            x.setText("X= ");
            x.setTextFill(point.getCircle().getFill());

            Label validationMessageX = new Label();
            validationMessageX.setText("");
            validationMessageX.setTextFill(Paint.valueOf("red"));
            validationMessageX.setLayoutX(20);
            validationMessageX.setLayoutY(80);

            TextField xValueField = new TextField();
            xValueField.setStyle(" -fx-font-weight: bold; -fx-font-size: 15; -fx-background-color: #9E9E9E; -fx-fill: #212121");
            xValueField.setLayoutX(60);
            xValueField.setMaxWidth(60);
            xValueField.setLayoutY(40);
            xValueField.setText(String.valueOf(point.getX()));

            String textValue = xValueField.getText();

            validate(validationMessageX, xValueField);

            Label y = new Label();
            y.setStyle(" -fx-font-weight: bold; -fx-font-size: 20 ");
            y.setLayoutX(130);
            y.setLayoutY(40);
            y.setText("Y= ");
            y.setTextFill(point.getCircle().getFill());

            Label validationMessageY = new Label();
            validationMessageY.setText("");
            validationMessageY.setTextFill(Paint.valueOf("red"));
            validationMessageY.setLayoutX(130);
            validationMessageY.setLayoutY(80);

            TextField yValueField = new TextField();
            yValueField.setStyle(" -fx-font-weight: bold; -fx-font-size: 15; -fx-background-color: #9E9E9E; -fx-fill: #212121");
            yValueField.setLayoutY(40);
            yValueField.setLayoutX(170);
            yValueField.setMaxWidth(60);
            yValueField.setText(String.valueOf(point.getY()));
            validate(validationMessageY, yValueField);

            pane.getChildren().addAll(title, x, xValueField, y, yValueField, validationMessageX, validationMessageY);

            Node nodeVBox = root.getChildren().get(0);
            if (nodeVBox instanceof VBox) {
                vbox = (VBox) nodeVBox;

                (vbox).getChildren().add(pane);
            }
        }
    }

    private void validate(Label validationMessageY, TextField yValueField) {
        yValueField.focusedProperty().addListener((arg0, oldValue, newValue) -> {
            if (!newValue) {
                if (yValueField.getText().matches(regex)) {
                    if (Double.parseDouble(yValueField.getText()) > Point.getMaxY()) {
                        validationMessageY.setText("INPUT OUTSIDE OF IMG");
                    } else {
                        validationMessageY.setText("");
                    }
                } else if (!yValueField.getText().matches(regex)) {
                    validationMessageY.setText("WRONG INPUT");
                }
            }
        });
    }


    public static void main(String[] args) {
        launch(args);
    }
}
