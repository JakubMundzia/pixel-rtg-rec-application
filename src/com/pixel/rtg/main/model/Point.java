package com.pixel.rtg.main.model;

import javafx.scene.shape.Circle;

import java.util.concurrent.atomic.AtomicInteger;

public class Point {

    private static final AtomicInteger count = new AtomicInteger(0);
    private String name;
    private double x;
    private double y;
    private Circle circle;
    private static final double MAX_X = 264.0;
    private static final double MAX_Y = 316.5;



    public Point(Point p) {
        this.name = p.getName();
        this.x = p.getX();
        this.y = p.getY();
        this.circle = p.getCircle();
    }

    public Point(double x, double y, Circle circle) {
        int id = count.incrementAndGet();
        this.name = "Point " + id;
        this.x = x;
        this.y = y;
        this.circle = circle;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static double getMaxX() {
        return MAX_X;
    }

    public static double getMaxY() {
        return MAX_Y;
    }
}
